<?php

namespace Drupal\user_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * User statistics controller.
 */
class UserStatisticsController extends ControllerBase {

  public const YEAR = 'year';
  public const MONTH = 'month';
  public const WEEK = 'week';
  public const DAY = 'day';

  /**
   * User statistics page callback.
   */
  public function content($interval = self::WEEK, $max_results = 50) {
    $threshold = date_create();

    $datediff = NULL;
    $threshold_string = '';
    $format_in = '';
    if ($interval === $this::YEAR) {
      $threshold_string = $threshold->format('Y-01-01 00:00:00');
      $format_in = 'Y-m-d H:i:s';
      $datediff = new \DateInterval('P1Y');
    }
    elseif ($interval === $this::MONTH) {
      $threshold_string = $threshold->format('Y-m-01 00:00:00');
      $format_in = 'Y-m-d H:i:s';
      $datediff = new \DateInterval('P1M');
    }
    elseif ($interval === $this::WEEK) {
      $tmp_date = date_create_from_format('Y-m-d H:i:s', $threshold->format('Y-m-d 00:00:00'));
      $one_day = new \DateInterval('P1D');
      while ($tmp_date->format('D') !== 'Mon') {
        $tmp_date->sub($one_day);
      }
      $threshold_string = $tmp_date->format('Y-z 00:00:00');
      $format_in = 'Y-z H:i:s';
      $datediff = new \DateInterval('P1W');
    }
    elseif ($interval === $this::DAY) {
      $threshold_string = $threshold->format('Y-z 00:00:00');
      $format_in = 'Y-z H:i:s';
      $datediff = new \DateInterval('P1D');
    }

    $table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Users until'),
        $this->t('User count'),
      ],
      '#rows' => [],
      '#empty' => $this->t('No data available.'),
    ];

    for ($i = 0; $i < $max_results; $i++) {
      // @todo Replace \Drupal::database with dependency injection.
      $count = \Drupal::database()->select('users_field_data', 'ufd')
        ->fields('ufd', ['uid'])
        ->condition('created', $threshold->getTimestamp(), '<=')
        ->condition('access', 0, '<>')
        ->countQuery()
        ->execute()
        ->fetchField();
      if (empty($count)) {
        break;
      }
      $table['#rows'][] = [
        $threshold->format('Y-m-d H:i:s'),
        $count,
      ];
      if ($i === 0) {
        $threshold = date_create_from_format($format_in, $threshold_string);
      }
      else {
        $threshold->sub($datediff);
      }
    }

    return $table;
  }

}
